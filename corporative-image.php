<?php
/**
 * Template Name: Corporative Image
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="website-intro section-intro padding-bottom__small-section padding-top__section">

        <div class="website-intro__circle" id="website-intro__circle"></div>

        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-11 col-md-5 text-color__white">
                    <?php if(get_field('corporative_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white line-height__medium--x" data-aos="fade-up" data-aos-delay="200"><?php the_field('corporative_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('corporative_title')): ?>
                        <h1 class="font-size__mega--x text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('corporative_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('corporative_text')): ?>
                        <div data-aos="fade-up" data-aos-delay="400">
                            <?php the_field('corporative_text'); ?>
                        </div>
                    <?php endif; ?>

                    <footer data-aos="fade-up" data-aos-delay="600">
                       <?php if(get_field('corporative_intro_cta_link')): ?>
                           <a href="<?php the_field('corporative_intro_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><i class="text-color__main margin-right__normal <?php the_field('corporative_intro_cta_icon'); ?>"></i><?php the_field('corporative_intro_cta_text'); ?></a>
                       <?php endif; ?>
                    </footer>
                </div>

                <div class="col-md-5 col-xs-12 col-md-offset-1">
                    <div class="ba-slider-wrapper hidden__xs">
                        <div class="before">
                            <div class="before-wrapper">
                                <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles line-height__medium--x"><?php _e('After', 'gh-apollo'); ?></h4>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/guruhotel-logo.svg">
                            </div>
                        </div>
                          <div class="after">
                            <div class="after-wrapper">
                                 <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles line-height__medium--x"><?php _e('Before', 'gh-apollo'); ?></h4>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-pixelated.png">
                            </div>


                          </div>
                          <div class="scroller">
                          </div>
                    </div>

                    <div class="hidden__md hidden__lg hidden__sm margin-top__mega--x corporative-image-comparison">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/corporative-image-comparison.svg">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(have_rows('corporative_services')) : ?>
        <section class="corporative-image-services padding__section">
            <div class="container-fluid wrap">

                <span class="or-deco background-color__main text-color__white"><?php _e('Or', 'gh-apollo'); ?></span>

                <div class="row">
                    <?php while(have_rows('corporative_services')): the_row(); ?>
                        <article class="item col-xs-10 col-xs-offset-1 col-md-4 col-md-offset-2">
                            <div class="img-wrapper margin-bottom__medium">
                                <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium_large']; ?>">
                            </div>

                            <h4><?php the_sub_field('title'); ?></h4>
                            <?php the_sub_field('text'); ?>

                            <?php if(get_sub_field('cta_link')): ?>
                                <a href="<?php the_sub_field('cta_link'); ?>" class="btn btn--primary border-radius__normal background-color__main text-color__white padding__medium--x margin-top__mega--x display__inline--block"><?php the_sub_field('cta_text'); ?></a>
                            <?php endif; ?>
                        </article>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if(have_rows('corporative_branding_list')) : ?>
        <section class="corporative-image-branding padding__section background-color__main">
            <div class="container-fluid wrap">
                <div class="row center-xs">
                     <div class="col-xs-11 col-md-5 start-xs">
                        <?php if(get_field('corporative_branding_preline')): ?>
                           <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white line-height__medium--x" data-aos="fade-up" data-aos-delay="200"><?php the_field('corporative_branding_preline'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('corporative_branding_title')): ?>
                            <h2 class="font-size__mega--x text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('corporative_branding_title'); ?></h2>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row center-xs">
                    <?php while(have_rows('corporative_branding_list')): the_row(); ?>
                        <article class="item col-xs-11 col-md-3 col-md-offset-1 text-color__white start-xs">

                            <?php if(get_sub_field('icon')): ?>
                                <span class="icon background-color__white display__block margin-bottom__big--x center-xs font-size__big">
                                    <i class="text-color__main <?php the_sub_field('icon'); ?>"></i>
                                </span>
                            <?php endif; ?>

                            <h4 class="text-color__white font-size__medium"><?php the_sub_field('title'); ?></h4>
                            <?php the_sub_field('text'); ?>
                        </article>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>


    <?php if(have_rows('corporative_branding_faq')) :  ?>
        <section class="corporative-image-faq padding__section text-color__titles">
            <div class="container-fluid wrap center-xs">
                <h2><?php the_field('corporative_branding_faq_title'); ?></h2>

                <div class="corporative-image-faq__list">
                    <div class="row center-xs">
                        <?php while(have_rows('corporative_branding_faq')): the_row(); ?>
                            <article class="item start-xs col-xs-11 col-sm-6 col-md-5 start-xs margin-bottom__big--x" data-aos="fade-up">
                                <div class="card border-radius__medium box-shadow__normal background-color__white padding__mega">
                                    <header>
                                        <h4 class="font-size__normal"><?php the_sub_field('title'); ?></h4>
                                        <button type="button" class="btn btn--primary background-color__main text-color__white box-shadow__normal"><i class="fa fa-plus"></i></button>
                                    </header>
                                    <div class="answer">
                                        <?php the_sub_field('answer'); ?>
                                    </div>
                                </div>

                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <section class="padding-top__section padding-bottom__mega--x background-color__grey corporative-image-forms" id="corporative-image-forms">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-5 start-xs">
                    <?php if(get_field('corporative_cta_title')): ?>
                        <h2 class="font-size__mega text-color__titles"><?php the_field('corporative_cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('corporative_cta_text')): ?>
                        <?php the_field('corporative_cta_text'); ?>
                    <?php endif; ?>

                    <ul class="form-selector margin-top__big">
                        <?php $i=0; if(have_rows('corporative_cta_forms')) : while(have_rows('corporative_cta_forms')): the_row(); ?>
                            <li <?php if($i==0) echo 'class="active"'; ?> data-form="form-<?php echo $i; ?>">
                                <i class="background-color__main text-color__white <?php the_sub_field('icon'); ?>"></i>
                                <button type="button"><?php the_sub_field('title'); ?></button>
                            </li>
                        <?php $i++; endwhile; endif; ?>
                    </ul>


                </div>

                <div class="col-xs-11 col-md-5 col-md-offset-1 start-xs">
                    <?php $i=0; if(have_rows('corporative_cta_forms')) : while(have_rows('corporative_cta_forms')): the_row(); ?>
                        <div class="corporative-image-forms__form <?php if($i==0) echo 'active'; ?>" data-form="form-<?php echo $i; ?>">
                            <?php echo do_shortcode( get_sub_field('form'), false ); ?>
                        </div>
                    <?php $i++; endwhile; endif; ?>
                </div>
            </div>
        </div>
    </section>



<?php endwhile; get_footer();
