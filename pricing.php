<?php
/**
 * Template Name: Product - Pricing
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="background-color__titles padding__section text-color__white section-intro">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-8">

                    <?php if(get_field('pricing_title')): ?>
                        <h1 class="font-size__mega text-color__white" data-aos="fade-up"><?php the_field('pricing_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('pricing_subtitle')): ?>
                        <h2 class="font-size__mega text-color__white font-weight__normal" data-aos="fade-up" data-aos-delay="50"><?php the_field('pricing_subtitle'); ?></h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <section class="pricing-explain">
        <div class="container-fluid wrap center-xs">
            <div class="row center-xs">
                <div class="col-sm-10">
                    <div class="card padding__small-section background-color__white border-radius__medium--x">
                        <h3 class="font-size__big--x text-color__main margin-bottom__big"><?php the_field('pricing_explain_title'); ?></h3>
                        <?php the_field('pricing_explain_text'); ?>

                        <div class="row margin-top__mega middle-xs">
                            <div class="col-xs-12 col-sm">
                                <h4 class="text-color__titles font-size__big margin-bottom__small font-weight__normal"><?php the_field('pricing_comission_title'); ?></h4>
                                <span class="font-size__small--x"><?php the_field('pricing_comission_text'); ?></span>
                            </div>
                            <div class="col-xs-12 col-sm-1 text-color__titles font-size__medium">
                                +
                            </div>
                            <div class="col-xs-12 col-sm">
                                <h4 class="text-color__titles font-size__big margin-bottom__small font-weight__normal"><?php the_field('pricing_payment_comission_title'); ?></h4>
                                <span class="font-size__small--x"><?php the_field('pricing_payment_comission_text'); ?></span>
                            </div>
                        </div>

                        <?php if(have_rows('pricing_explain_payment_methods_icons')) : ?>
                            <div class="pricing-explain__payment-methods-icons font-size__big--x margin-top__mega">
                                <?php while(have_rows('pricing_explain_payment_methods_icons')): the_row(); ?>
                                    <i class="<?php the_sub_field('icon'); ?>"></i>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>

                        <?php if(have_rows('pricing_explain_features')) : ?>
                            <div class="pricing-explain__features margin-top__mega text-color__titles">
                                <ul>
                                    <?php while(have_rows('pricing_explain_features')): the_row(); ?>
                                        <li data-aos="fade-up"><i class="icon text-color__main <?php the_sub_field('icon'); ?>"></i> <?php the_sub_field('text'); ?></li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <hr class="margin-top__mega--x margin-bottom__mega--x">

                        <h4 class="text-color__titles margin-bottom__big"><?php the_field('pricing-explain__payment-methods-title'); ?></h4>

                        <?php if(have_rows('pricing_explain_payment_methods')) : ?>
                            <div class="pricing-explain__payment-methods">
                                <?php while(have_rows('pricing_explain_payment_methods')): the_row(); ?>
                                   <div class="card background-color__white border-radius__normal box-shadow__medium display__inline--block margin-bottom__big  padding__mega--x">
                                       <div class="row start-xs middle-xs">
                                           <div class="col-xs-12 col-sm-6 col-md-6">
                                               <img src="<?php $img = get_sub_field('logo'); echo $img['sizes']['medium']; ?>">
                                               <div class="font-size__small--x text-color__titles"><?php the_sub_field('text'); ?></div>
                                           </div>
                                           <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2 center-xs">
                                               <span class="text-color__titles display__block font-size__medium--x"><?php the_sub_field('comission'); ?></span>
                                               <a href="<?php the_sub_field('link'); ?>" target="_blank" class="font-weight__normal text-color__main font-size__small--x display__block"><?php the_sub_field('link_text'); ?></a>
                                           </div>
                                       </div>
                                   </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>

                        <?php if(get_field('pricing-explain__payment-methods-acclaration')): ?>
                            <div class="text-color__titles"><?php the_field('pricing-explain__payment-methods-acclaration'); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <?php if(have_rows('pricing-faq')) :  ?>
        <section class="pricing-faq padding__small-section text-color__titles">
            <div class="container-fluid wrap center-xs">
                <h2><?php _e('Frequently asked questions', 'guru'); ?></h2>

                <div class="pricing-faq__list margin-top__mega">
                    <div class="row">
                        <?php while(have_rows('pricing-faq')): the_row(); ?>
                            <article class="item start-xs col-xs-12 col-sm-6 col-md-5 col-md-offset-1" data-aos="fade-up">
                                <h4 class="font-size__medium margin-bottom__mega--x"><?php the_sub_field('question'); ?></h4>
                                <?php the_sub_field('answer'); ?>
                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <section class="padding__small-section pricing-calculator padding-bottom__mega--x background-color__white">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-12 col-md-6">

                    <?php if(get_field('pricing_calculator_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__main line-height__medium--x" data-aos="fade-up" data-aos-delay="200"><?php the_field('pricing_calculator_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('pricing_calculator_title')): ?>
                        <h2 class="font-size__big--x" data-aos="fade-up"><?php the_field('pricing_calculator_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(have_rows('pricing_calculator_features')) : ?>
                        <div class="pricing-calculator__features margin-top__big">
                            <ul>
                                <?php while(have_rows('pricing_calculator_features')): the_row(); ?>
                                    <li data-aos="fade-up"><i class="icon text-color__main <?php the_sub_field('icon'); ?>"></i> <?php the_sub_field('text'); ?></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-5 center-xs">
                    <div class="card background-color__white border-radius__medium box-shadow__medium display__inline--block margin-bottom__big  padding__mega--x">
                        <p class="text-color__titles"><?php the_field('pricing_calculator_card_title'); ?></p>

                        <div class="margin-top__big input-wrapper margin-bottom__mega--x">
                            <input type="number" value="100" class="border-color__utilitary border-radius__normal" id="pricing-calculator-user-input" min="10">
                            <span class="error text-color__red margin-top__small font-size__small--x">The value must be greater than 10.</span>
                        </div>

                        <div class="pricing-calculator__comissions">
                            <?php if(have_rows('pricing_payment_methods_comissions')) : while(have_rows('pricing_payment_methods_comissions')): the_row(); ?>
                                <article class="item">
                                    <div class="row middle-xs">
                                        <div class="col-xs-4 start-xs">
                                            <img src="<?php $img = get_sub_field('logo'); echo $img['sizes']['medium']; ?>">
                                        </div>
                                        <div class="col-xs-8 end-xs">
                                            <span class="font-size__small--x display__inline--block margin-right__normal"><?php _e('You get', 'guru'); ?></span>
                                            <span class="text-color__titles display__inline--block font-weight__bold font-size__medium pricing-result" data-percentage="<?php the_sub_field('percentage_comission'); ?>" data-fixed="<?php the_sub_field('fixed_comission'); ?>">$95</span>
                                            <div class="display__inline--block tooltip"><i class="fas fa-question-circle question font-size__small--x margin-left__normal"></i><span class="tooltip-text"><?php the_sub_field('tooltip'); ?></span></div>
                                        </div>
                                    </div>
                                </article>
                            <?php endwhile; endif; ?>
                        </div>

                        <span class="display__block margin-top__big text-color__main font-size__small--x font-weight__normal"><?php _e('Plus an extra saving of', 'guru'); ?></span>

                        <div class="text-color__titles"><span class="font-size__small--x">-$</span><span class="font-size__big--x font-weight__bold"><?php the_field('pricing_additional_savings'); ?></span><span class="font-size__normal font-weight__bold"><?php _e('/ Mo', 'guru'); ?></span></div>

                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/underline-decoration.svg" class="underline-decoration display__block">

                        <p class="text-color__titles font-size__small--x"><?php the_field('pricing_additional_savings_desc'); ?></p>

                        <div class="text-color__white background-color__titles note border-radius__small--x font-size__small--x padding__small margin-top__mega--x display__inline--block"><?php the_field('pricing_estimation_text'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php if (get_field('show_quotes')): ?>

        <section id="home-quotes" class="background-color__titles position__relative">
            <div class="container-fluid wrap">
                <div class="row middle-xs center-xs">
                    <div class="col-xs-10">
                        <div class="quotes-logos">
                            <?php
                                $i = 1;
                                $args = array(
                                  'post_type'      => 'guru_case_study',
                                  'posts_per_page' => 8,
                                );
                                $query = new WP_Query( $args );
                                if($query->have_posts()): while($query->have_posts()) : $query->the_post();
                            ?>
                                <a href="#">
                                    <img src="<?php $img = get_field('logo'); echo $img['sizes']['medium']; ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*150; ?>">
                                </a>
                            <?php $i++; endwhile; endif; ?>
                        </div>

                        <div class="quotes-slider">
                            <?php
                                while($query->have_posts()) : $query->the_post();
                            ?>
                                <div class="quotes-slider__slide background-color__white border-radius__medium--x left-xs">
                                    <div class="row">
                                        <div class="col-xs-10 order-xs__2 order-sm__2 col-xs-offset-2 col-md-7 col-md-offset-1 padding-top__mega--x padding-bottom__mega--x quotes-slider__content">
                                            <blockquote class="without-margin__left position__relative">
                                                <?php echo strip_tags(get_the_content(
                                            )); ?>
                                            </blockquote>

                                            <footer><h6 class="display__inline--block"><?php the_field('name'); ?></h6> - <h6 class="display__inline--block"><?php the_title(); ?></h6></footer>
                                        </div>
                                        <div class="col-xs-12 order-xs__1 order-sm__1 col-md-4 position__relative quotes-slider__images">

                                            <?php the_post_thumbnail('medium_large', array('class'=> 'quotes-slider__img--hotel')); ?>

                                            <img src="<?php $img = get_field('logo'); echo $img['sizes']['medium']; ?>" class="quotes-slider__img--logo" data-aos="zoom-in">
                                        </div>
                                    </div>
                                </div>
                           <?php endwhile; wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif ?>

        <section id="home__cta" class="padding-top__small-section padding-bottom__mega--x background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-md-6">

                    <?php if (get_field('pricing_cta_title_img')): ?>

                        <img src="<?php $img = get_field('pricing_cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                    <?php elseif(get_field('pricing_cta_title')): ?>
                        <h2 class="font-size__mega" data-aos="fade-up"><?php the_field('pricing_cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('pricing_cta_text')): ?>
                        <?php the_field('pricing_cta_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('pricing_cta_cta_link')): ?>
                        <a href="<?php the_field('pricing_cta_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('pricing_cta_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; get_footer();
