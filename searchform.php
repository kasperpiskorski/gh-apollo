<form action="/" method="get" class="search-form">
    <input type="text" name="s" class="border-radius__normal display__inline--block font-size__small--x" placeholder="Escribe aquí..." id="search" value="<?php the_search_query(); ?>" />
    <button type="submit" alt="Enviar" value="Enviar" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block font-size__small--x">
        <i class="fa fa-search"></i> Buscar
    </button>
</form>
